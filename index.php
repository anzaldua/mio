 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Prueba Alex</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="vistas/img/favicon.png" rel="icon">
  <link href="vistas/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="vistas/https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="vistas/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="vistas/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="vistas/lib/animate/animate.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="vistas/css/style.css" rel="stylesheet">

  <!-- Fancy css CDN -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

</head>

<body>
    
    <?php 
        include "vistas/cabezote.php";
        include "vistas/inicio.php";
    ?>

    <!-- JavaScript Libraries -->
  <script src="vistas/lib/jquery/jquery.min.js"></script>
  <script src="vistas/lib/jquery/jquery-migrate.min.js"></script>
  <script src="vistas/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vistas/lib/easing/easing.min.js"></script>
  <script src="vistas/lib/wow/wow.min.js"></script>
  <script src="vistas/lib/waypoints/waypoints.min.js"></script>
  <script src="vistas/lib/counterup/counterup.min.js"></script>
  <script src="vistas/lib/superfish/hoverIntent.js"></script>
  <script src="vistas/lib/superfish/superfish.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="vistas/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="vistas/js/main.js"></script>

  <!-- Fancy js CDN -->
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>

</body>
</html>